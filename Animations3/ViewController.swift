//
//  ViewController.swift
//  Animations3
//
//  Created by EuvicDev on 03/08/2017.
//  Copyright © 2017 EuvicDev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var addItemView: UIView!
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!

    @IBOutlet weak var lbl1: UILabel!
    @IBOutlet weak var lbl2: UILabel!
    @IBOutlet weak var lbl1Constraint: NSLayoutConstraint!
    @IBOutlet weak var lbl2Constraint: NSLayoutConstraint!

    var effect: UIVisualEffect!

    override func viewDidLoad() {
        super.viewDidLoad()

        //we dont need visual efect for now.
        effect = visualEffectView.effect
        self.visualEffectView.effect = nil

        addItemView.layer.cornerRadius = 10
        self.lbl1Constraint.constant -= view.bounds.width
        self.lbl2Constraint.constant += view.bounds.width
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        fadeTextField(alpha: 1, duration: 2.0, delay: 3)
    }

    func animateIn() {
        self.view.addSubview(addItemView)
        addItemView.center = self.view.center
        addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        addItemView.alpha = 0
        UIView.animate(withDuration: 0.4) {
            self.visualEffectView.effect = self.effect
            self.addItemView.alpha = 1
            self.addItemView.transform = CGAffineTransform.identity
        }
    }

    func animateOut() {
        UIView.animate(withDuration: 0.4, animations: {
            self.addItemView.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.addItemView.alpha = 0
            self.visualEffectView.effect = nil
        }, completion: {
            (success: Bool) in
            self.addItemView.removeFromSuperview()
        })
    }

    func fadeTextField(alpha: CGFloat, duration: Double = 1.0, delay: Double = 0) {
        UILabel.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.titleLabel.alpha = alpha
            self.lbl1.alpha = alpha
            self.lbl2.alpha = alpha
        }, completion: nil)
    }

    func linesAnimation(duration: Double = 1.0, delay: Double = 0) {
        UILabel.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.lbl1Constraint.constant += self.view.bounds.width
            self.lbl2Constraint.constant -= self.view.bounds.width
            self.view.layoutIfNeeded()
        }, completion: nil)


    }

    @IBAction func addItem(_ sender: Any) {
        fadeTextField(alpha: 0, duration: 0.2)
        animateIn()
    }

    @IBAction func dismissPopUp(_ sender: Any) {
        animateOut()
        fadeTextField(alpha: 1, duration: 0.2)
        linesAnimation()
        linesAnimation(duration: 1, delay: 1.0)
        titleLabel.text = titleTextField.text

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

